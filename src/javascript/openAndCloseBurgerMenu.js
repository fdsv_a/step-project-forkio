"use strict";

const headerBurgerBtn = document.querySelector('.btn--burger');
const headerNav = document.querySelector('.header__navigation');
const burgerMenuImg = [...document.querySelectorAll('.btn__img')];
const burgerOpenImg = document.querySelector(`[data-burger="open"]`);
const burgerCloseImg = document.querySelector(`[data-burger="close"]`);

document.body.addEventListener('click', (e) => {
    if (!e.target.closest('.header__item')) {
        headerNav.classList.add('header__navigation--hidden');
        burgerOpenImg.classList.remove('btn__img--hidden');
        burgerCloseImg.classList.add('btn__img--hidden');
    }
})

headerBurgerBtn.addEventListener('click', (e) => {
    e.stopPropagation();
    headerNav.classList.toggle('header__navigation--hidden');
    burgerMenuImg.forEach(img => img.classList.toggle('btn__img--hidden'));
})



