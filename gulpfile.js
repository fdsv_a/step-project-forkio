import autoprefixer from "gulp-autoprefixer";
import clean from "gulp-clean";
import cleanCss from "gulp-clean-css";
import imagemin from "gulp-imagemin";
import minifyjs from "gulp-js-minify";
import gulp from "gulp";
import browserSync from "browser-sync";
import uglify from "gulp-uglify";
import concat from "gulp-concat";
import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);

const BS = browserSync.create();

export const buildStyles = () =>
  gulp.src("./src/**/*.scss").pipe(sass()).pipe(gulp.dest("./dist/css"));

const buildJS = () =>
  gulp
    .src("./src/javascript/*.js")
    // .pipe(minifyjs())
    .pipe(gulp.dest("./dist/javascript"));

const moveImages = () =>
  gulp.src("./src/image/**/*").pipe(imagemin()).pipe(gulp.dest("./dist/image"));

const cleaner = () => gulp.src("./dist/**/*").pipe(clean());

const build = gulp.series(cleaner, buildStyles, buildJS, moveImages);

export const dev = gulp.series(build, () => {
  BS.init({
    server: {
      baseDir: "./",
    },
  });

  gulp.watch(
    "./src/**/*",
    gulp.series(build, (done) => {
      BS.reload();
      done();
    })
  );
});
