'use strict';

const headerNavItems = document.querySelector('.header__list');

headerNavItems.addEventListener('click', (e) => {
    let previousActiveTarget = document.querySelector(".header__item.header__item--active");
    if(e.target !== e.currentTarget && e.target.classList.contains('header__item')) {
        previousActiveTarget.classList.remove('header__item--active');
        e.target.classList.add('header__item--active');
    }
})